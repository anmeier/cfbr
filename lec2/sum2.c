// sum2.c
// Author: Dongwook Shin
// This program adds two numbers and prints the results to the screen

#include <stdio.h>

int main()
{ 
	double  dX, dY, dZ;  // double precision variables

	dX = 1.0/3.0;
	dY = 5.7;
	dZ = dX + dY;

	printf("X = %10.6lf, Y = %10.6lf, Z = X + Y = %10.6lf\n", dX, dY, dZ);

	return (0);

} // end of main

// end of sum2.c  
