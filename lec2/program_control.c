// program_control.c
// Author: Dongwook Shin
// This program illustrates the syntax of assignments, if ... then statements
// looping in C

#include <stdio.h>
//#include <math.h>
//#include <stdlib.h>

main()
{ 
  int  i, j, k;                   // integer looping variables
  int  iThree = 3;                // integer variable declaration and assignment
  double dZero, dY;               // double precision variables 
  const double PI = 3.141592654;  // double precision constant
                                  // the program will generate an error if an
                                  // attempt is made to change the value of PI



  dZero = dY = 0.0;               // both variables are assigned the value 0
                                  // assignments are executed from right to left
                                  /* comments can also be done this way */

  //
  // arithmetic statements
  //
  printf("\niThree = %d\n", iThree);

  iThree = iThree + 2;
  printf("iThree = %d\n", iThree);

  iThree += 2;    //  shorthand for iThree = iThree + 2
  printf("iThree = %d\n", iThree);

  iThree *= 3;    //  shorthand for iThree = iThree * 3
  printf("iThree = %d\n", iThree);

  iThree++;       //  shorthand for iThree = iThree + 1
  printf("iThree = %d\n", iThree);

  iThree--;       //  shorthand for iThree = iThree - 1
  printf("iThree = %d\n", iThree);

  // more on ++
  // hint: don't write code where the order of ++ matters!
  printf("\nMore on ++\n");
  printf("iThree = %d\n", iThree);
  i = ++iThree;    // increments iThree first then assigns to i
  printf("i = %d, iThree = %d\n", i, iThree);
 
  printf("iThree = %d\n", iThree);
  i = iThree++;    // assigns iThree to i and then increments iThree
  printf("i = %d, iThree = %d\n", i, iThree);

  // the modulus (%) operator
  printf("\nThe modulus (%%) operator\n");
  i = 4;
  j = 11;
  k = j % i;   // k is the remainder when j is divided by i
  printf("i = %d, j = %d, k = %d\n", i, j, k);

  //
  // if then else statements
  //
  printf("\nIf then else statements\n");
  iThree = 3;
  if (iThree == 3)  // test for equality
     printf("iThree == 3 is true\n");
    
  if (iThree != 3)  // test for inequality            
     printf("iThree != 3 is true\n");

  if (iThree >= 3)
  {  // statements can be grouped using {   } 
     printf("iThree >= 3 is true\n");
     printf("you can execute multiple statements\n");
  }

  if (iThree >= 3 && dZero > 1.0)   // && is the AND condition
      printf("iThree >= 3 && dZero <= 0.0 is true\n");
  else
      printf("iThree >= 3 && dZero <= 0.0 is false\n");
    
  if (iThree >= 3 || dZero > 1.0)   // || is the OR condition
      printf("iThree >= 3 || dZero <= 0.0 is true\n");
  else
      printf("iThree >= 3 || dZero <= 0.0 is false\n");

  i = 3; j = 4;
  if (i > j)
    k = i;
  else
    k = j;
  printf("i = %d, j = %d, k = %d\n", i, j, k);

  // here is another way to write this using the ? operator
  k = (i > j) ? i : j;
  printf("i = %d, j = %d, k = %d\n", i, j, k);

  //
  // for loops
  //
  printf("\nFor loops\n");
  for (i = 1; i <= 3; i++)
    printf("i = %d\n", i);

  for (i = 5; i <= 3; i++)
  { 
      printf("Do loops execute once? \n", i);
      printf("i = %d\n", i);
  }

  for (i = 1, j = 20; (i <= 15 && j > 3); i = i + 3)
  {
    printf("i = %d, j = %d\n", i, j);
    j = j - 5; 
  }  

  i = 1;                 // initialization inside the for loop is optional
  for (; i <= 3; i++)    // but the first ; is required as a placeholder
    printf("i = %d\n", i);  

  //
  // while loops
  //
  printf("\nWhile loops\n");
  i = 10;
  while (i > 6)
  {
     printf("inside the while loop i = %d\n", i);
     i--;    
  }
  printf("after the while loop i = %d\n", i);

  do 
  {
      i++;    
      printf("inside the do loop i = %d\n", i);
  }  while (i <= 8);
  printf("after the do loop i = %d\n", i);
  
  // exit loops early using the break statement
  printf("\nBreak statement\n");
  for (i = 1; i <= 3; i++)
  { 
      printf("inside the for loop i = %d\n", i);
      if (i == 2) break;
  }
  printf("after the for loop i = %d\n", i);

  i = 10;
  while (i > 6)
  {
     if (i == 8) break;
     printf("inside the while loop i = %d\n", i);
     i--;    
  }
  printf("after the while loop i = %d\n", i);

  return (0);

} // end of main


// end of program_control.c