// pointers.c
// Author: Dongwook Shin
// This program illustrates the use of pointers in C

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

void swap1(double dX, double dY);
void swap2(double *pdX, double *pdY);

int main()
{ 
  double dX, dY;   // double precision variables
  double *pdX;     // pointer to a double precision variable
  double dVec[5];  // creates an array of doubles dVec[0], ..., dVec[4]
  double *pdVec;   // creates a pointer to an array of doubles
  double dVec1[5]; // creates an array of doubles dVec1[0], ..., dVec1[4]
  double *pdVec1;
  int    i;

  dX = 5.0;
  pdX = &dX;   // the operator & assigns the address of dX to pdX
               // pdX is said to point to dX

  // think of variables and pointers by analogy to a spreadsheet
  //  range name  variable   value    address     program
  //                dX        5        pdX           C
  //     dX                   5       cell B5      spreadsheet

  dY = dX + 2.0;   // assigns the value dX+2 to dY
  printf("dY = %lf\n", dY);
  
  dY = *pdX + 2.0; // the 'indirection' or 'dereferencing' operator * 
                   // accesses the object that pdX points to,
                   // so 'dY = dX + 2' and dY = *pdX + 2; do the same thing 
  printf("dY = %lf\n", dY);

  *pdX = 12.0;     // this has the same effect as X = 12.0 
  printf("dX = %lf\n", dX);
  
  //
  // C functions pass arguments by value, so after calling the routine swap1
  // the values of dX and dY are NOT changed
  //
  printf("\nBefore swap 1: dX = %lf, dY = %lf\n", dX, dY);
  swap1(dX, dY);
  printf("After swap 1:  dX = %lf, dY = %lf\n", dX, dY);

  //
  // Defining the arguments of the function to be pointers (addresses)
  // tells C to swap the values of the double variables that are pointed to.
  // So after calling the routine swap2 the values of dX and dY ARE changed.
  //
  printf("\nBefore swap 2: dX = %lf, dY = %lf\n", dX, dY);
  swap2(&dX, &dY);
  printf("After swap 2:  dX = %lf, dY = %lf\n", dX, dY);

  //
  // arrays and pointers
  //
  printf("\nArrays and pointers\n");

  // assign some values to the arrays dVec and dVec1
  for (i = 0; i < 5; i++)
  {
      dVec[i] = 100.0*(i+3.0);
      dVec1[i] = -35.0*i;
  }

  pdVec = &dVec[0];   // sets pdVec to point to the first element of dVec
                      // i.e., pdVec contains the address of dVec[0].

                      // The name of an array is the same as the location
                      // of its first element, so &dVec[0] and dVec are the same.
                      // So pdVec = &dVec[0] is the same as pdVec = dVec.

  dY = dVec[0] + 2.0; // assigns the value dVec[0]+2 to dY
  printf("dY = %lf\n", dY);

  dY = *pdVec + 2.0;  // also assigns the value dVec[0]+2 to dY
  printf("dY = %lf\n", dY);

  //
  // swap arrays pdVec and pdVec1
  //
  // here is the wrong way (i.e., it works, but is slow):
  //  for (i = 0; i < 5; i++)
  //  {
  //      dX       = dVec1[i];
  //      dVec1[i] = dVec[i];
  //      dVec[i]  = dX;
  //  }

  pdVec  = &dVec[0];   // since &dVec[0] is the same as dVec, this is the same as pdVec = dVec
  pdVec1 = &dVec1[0];

  printf("\nBefore swapping pointers: \n");
  printf("dVec[3] = %lf, dVec1[3] = %lf\n", dVec[3], dVec1[3]); 
  printf("dVec[3] = %lf, dVec1[3] = %lf\n", *(pdVec+3), *(pdVec1+3)); 
  printf("pdVec[3] = %lf, pdVec1[3] = %lf\n", pdVec[3], pdVec1[3]); 
  printf("pdVec[4] = %lf, pdVec1[4] = %lf\n", pdVec[4], pdVec1[4]); 

  // here is the correct way by swapping pointers
  // swapping the pointers avoids swapping the data in long arrays
  pdX    = pdVec1;
  pdVec1 = pdVec;
  pdVec  = pdX;

  printf("After swapping pointers: \n");
  printf("pdVec[3] = %lf, pdVec1[3] = %lf\n", pdVec[3], pdVec1[3]); 
  printf("pdVec[4] = %lf, pdVec1[4] = %lf\n", pdVec[4], pdVec1[4]); 

  return (0);

} // end of main


void swap1(double dX, double dY)
{
   double dTmp;

   dTmp = dX;
   dX   = dY;
   dY   = dTmp;
 
   return;

}  // end of function swap1  

void swap2(double *pdX, double *pdY)
{
   double dTmp;

   dTmp = *pdX;
   *pdX   = *pdY;
   *pdY   = dTmp;
 
   return;

}  // end of function swap2  

// end of pointers.c