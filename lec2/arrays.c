// arrays.c
// Author: Dongwook Shin
// This program illustrates the use of the Numerical Recipes
// routines for creating and destroying vectors and matrices

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

void     print_mat(char *name, double **ppdEMat, int iMin, int iMax, 
                       int jMin, int jMax);
void     print_vec(char *name, double *pdEVec, int iMin, int iMax);
void     mat_vec_mult( double *pdCVec, double **ppdAMat, double *pdBVec, 
                       int iRow, int iCol);
void     nrerror( char error_text[] );
double   *dvector( long nl, long nh );
void     free_dvector( double *v, long nl, long nh );
double   **dmatrix( long nrl, long nrh, long ncl, long nch );
void     free_dmatrix( double **m, long nrl, long nrh, long ncl, long nch );

int main()
{ 
  double   **ppdAMat, **ppdBMat;   // double precision matrices
  double   *pdAVec, *pdBVec;       // double precision vectors
  int      iRow, iCol;             // row and column sizes 
  int      i, j;                   // looping variables
 
  iRow = 2;
  iCol = 3;
  
  // create matrices and vectors
  // each dmatrix call should be paired with a call to free_dmatrix
  // each dvector call should be paired with a call to free_dvector
  ppdAMat  = dmatrix( 1, iRow, 1, iCol );
  ppdBMat  = dmatrix( 1, iRow, 1, iCol );
  pdAVec   = dvector( 1, iRow );
  pdBVec   = dvector( 1, iCol );

  // initialize the matrices and vectors
  for (i = 1; i <= iRow; i++)
  { 
     pdAVec[i] = 1.0 / i;
  	 for (j = 1; j <= iCol; j++)
     { 
         ppdAMat[i][j] = 1.0 / (i + j - 1.0);
         ppdBMat[i][j] = i + j;
     }
  }
 
  for (j = 1; j <= iCol; j++)
      pdBVec[j] = j;

  // print results to the screen
  print_mat("ppdAMat", ppdAMat, 1, iRow, 1, iCol );
  print_vec("pdBVec",   pdBVec, 1, iCol );

  // matrix vector multiplication example
  mat_vec_mult( pdAVec, ppdAMat, pdBVec, iRow, iCol);
  printf("Result after A*b:\n");
  print_vec("pdAVec",   pdAVec, 1, iRow );
  
  // free memory 
  free_dmatrix( ppdAMat, 1, iRow, 1, iCol );
  free_dmatrix( ppdBMat, 1, iRow, 1, iCol ); 
  free_dvector( pdAVec, 1, iRow );
  free_dvector( pdBVec, 1, iRow );

  // illustrates the use of alternate row and column indexing
  ppdAMat  = dmatrix( -iRow, iRow, iCol, 2*iCol-1 );
  for (i = -iRow; i <= iRow; i++)
  { 
  	 for (j = iCol; j <= 2*iCol-1; j++)
         ppdAMat[i][j] = 1.0 / (i + j + 1.0);
  }

  print_mat("ppdAMat", ppdAMat, -iRow, iRow, iCol, 2*iCol-1 );
  free_dmatrix( ppdAMat, -iRow, iRow, iCol, 2*iCol-1 );

  return (0);

} // end of main

/**********************************************************************/
void print_mat(char *name, double **ppdEMat, int iMin, int iMax, 
                int jMin, int jMax)
{
  // a simple routine to print the matrix ppdEMat 
  // with row and column headers

  int i, j;

  printf("%s\n ", name);
  for (i = iMin; i <= iMax; i++)     
  { 
      if (i == iMin)
	  { 
         for (j = jMin; j <= jMax; j++) printf("%6d ", j);
	     printf("\n");
	  }

      printf("%2d ", i);
      for (j = jMin; j <= jMax; j++) 
          printf("%6.2lf ", ppdEMat[i][j]);
      printf("\n");
  }
  printf("\n");
 
  return;

}  // end of print_mat

/**********************************************************************/
void print_vec(char *name, double *pdEVec, int iMin, int iMax)
{
  int i;

  printf("%s\n", name);
  for (i = iMin; i <= iMax; i++)
    { printf("%3d  %7.4lf ", i, pdEVec[i]);
      printf("\n");
    }
  printf("\n");
  return;
  
}  // end of print_vec

void mat_vec_mult( double *pdCVec, double **ppdAMat, double *pdBVec, int iRow, int iCol )
{
  // a simple matrix*vector multiplication routine 
  // Inputs: the input matrix A must be indexed [1..iRow][1..iCol]
  //         and the input vector B is indexed [1..iCol]
  // Output: the output vector C is indexed [1..iRow] 

  int j, k;

  for (k = 1; k <= iRow; k++)     
  { 
      pdCVec[k] = 0.0;
      for (j = 1; j <= iCol; j++) 
         pdCVec[k] += ppdAMat[k][j]*pdBVec[j];
  }
 
  return;

}  // end of mat_vec_mult

/**********************************************************************/
void nrerror( char error_text[] )
{
  // Numerical Recipes standard error handler
  fprintf( stderr,"Numerical Recipes run-time error...\n" );
  fprintf( stderr,"%s\n",error_text );
  fprintf( stderr,"...now exiting to system...\n" );
  exit(1);

} // end of nrerror

/**********************************************************************/
double *dvector( long nl, long nh )
{
  // allocate a double vector with subscript range v[nl..nh]

  double *v;

  v=(double *)malloc((size_t) ((nh-nl+2)*sizeof(double)));
  if (!v) nrerror("allocation failure in dvector()");
  return v-nl+1;

} // end of dvector

/**********************************************************************/
void free_dvector( double *v, long nl, long nh )
{
  // free a double vector allocated with dvector()

  free((char*) (v+nl-1));

} // end of free_dvector

/**********************************************************************/
double **dmatrix( long nrl, long nrh, long ncl, long nch )
{
  // allocate a double matrix with subscript range m[nrl..nrh][ncl..nch]

  long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
  double **m;

  // allocate pointers to rows
  m=(double **) malloc((size_t)((nrow+1)*sizeof(double*)));
  if (!m) nrerror("allocation failure 1 in dmatrix()");
  m += 1;
  m -= nrl;

  // allocate rows and set pointers to them
  m[nrl]=(double *) malloc((size_t)((nrow*ncol+1)*sizeof(double)));
  if (!m[nrl]) nrerror("allocation failure 2 in dmatrix()");
  m[nrl] += 1;
  m[nrl] -= ncl;

  for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;

  // return pointer to array of pointers to rows
  return m;

} // end of dmatrix 

/**********************************************************************/
void free_dmatrix( double **m, long nrl, long nrh, long ncl, long nch )
{
  // free a double matrix allocated by dmatrix()

  free((char*) (m[nrl]+ncl-1));
  free((char*) (m+nrl-1));

} // end of free_dmatrix

// end of arrays.c  
