#include <stdio.h>

/*
 * From http://stackoverflow.com/questions/20763616/how-many-bytes-do-pointers-take-up
 */

int main() {
  printf("%ld bytes per pointer\n", sizeof(void *));
}
