// loops.c
// Author: Dongwook Shin & Dan Mechanic
// looping in C

#include <stdio.h>

int main()
{ 
  int  i, j;                   // integer looping variables
  //
  // for loops
  //
  printf("\nFor loops\n");
  for (i = 1; i <= 3; i++)
    printf("i = %d\n", i);

  
  for (i = 5; i <= 3; i++)
  { 
      printf("Do loops execute once? \n");
      printf("i = %d\n", i);
  }

  for (i = 1, j = 20; (i <= 15 && j > 3); i = i + 3)
  {
    printf("i = %d, j = %d\n", i, j);
    j = j - 5; 
  }  

  i = 1;                 // initialization inside the for loop is optional
  for (; i <= 3; i++)    // but the first ; is required as a placeholder
    printf("i = %d\n", i);  

  for (int idan=0; idan<5; idan++){
    printf("idan = %d\n", idan);
  }

  // idan is OUT OF SCOPE
  //    the line below would throw a compiler error
  // printf("idan = %d\n", idan);

  
  //
  // while loops
  //
  printf("\nWhile loops\n");
  i = 10;
  while (i > 6)
  {
     printf("inside the while loop i = %d\n", i);
     i--;    
  }
  printf("after the while loop i = %d\n", i);

  do 
  {
      i++;    
      printf("inside the do loop i = %d\n", i);
  }  while (i <= 8);
  printf("after the do loop i = %d\n", i);
  
  // exit loops early using the break statement
  printf("\nBreak statement\n");
  for (i = 1; i <= 3; i++)
  { 
      printf("inside the for loop i = %d\n", i);
      if (i == 2) break;
  }
  printf("after the for loop i = %d\n", i);

  i = 10;
  while (i > 6)
  {
     if (i == 8) break;
     printf("inside the while loop i = %d\n", i);
     i--;    
  }
  printf("after the while loop i = %d\n", i);

  return (0);

} // end of main


// end of program_control.c
