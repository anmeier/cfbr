// conditionals.c
// Author: Dongwook Shin & Dan Mechanic
// This program illustrates the syntax of conditionals
// in C

#include <stdio.h>

int main()
{ 
  int  i, j, k;                   // integer looping variables
  int  iThree = 3;                // integer variable declaration and assignment
  double dZero, dY;               // double precision variables 

  dZero = dY = 0.0;               // both variables are assigned the value 0
                                  // assignments are executed from right to left
                                  /* comments can also be done this way */

  //
  // if then else statements
  //
  printf("\nIf then else statements\nWe start with:\niThree = 3;\n\n");
  iThree = 3;
  if (iThree == 3)  // test for equality
     printf("iThree == 3 is true\n");
    
  if (iThree != 3)  // test for inequality            
     printf("iThree != 3 is true\n");

  if (iThree >= 3)
  {  // statements can be grouped using {   } 
     printf("iThree >= 3 is true - ");
     printf("you can execute multiple statements\n");
  }

  if (--iThree >= 3)
  {  // statements can be grouped using {   } 
     printf("iThree >= 3 is true - ");
     printf("you can execute multiple statements\n");
  } else {
     printf("--iThree >= 3 is false -");
     printf("you can execute multiple statements\n");
  }

  printf("Resetting iThree\niThree = 3;\n");
  iThree = 3;
  if (iThree >= 3 && dZero > 1.0)   // && is the AND condition
      printf("iThree >= 3 && dZero <= 0.0 is true\n");
  else
      printf("iThree >= 3 && dZero <= 0.0 is false\n");

  if (iThree >= 3 && ++iThree >= 3)   // && is the AND condition
    printf("\n\niThree >= 3 && ++iThree >= 3 is true\niThree is now %d\n\n\n",iThree);
  else
      printf("iThree >= 3 && dZero <= 0.0 is false\n");

  printf("\n\nResetting iThree\niThree = 3;\n");
  
  if (iThree >= 3 || dZero > 1.0)   // || is the OR condition
      printf("iThree >= 3 || dZero <= 0.0 is true\n");
  else
      printf("iThree >= 3 || dZero <= 0.0 is false\n");

  if (iThree >= 3 || iThree++ >= 3)   // || is the OR condition
      printf("iThree >= 3 || iThree++ >= 3\niThree is now %d\n\n\n",iThree);
  else
      printf("iThree >= 3 || dZero <= 0.0 is false\n");
  
  
  printf("i = 3; j = 4;\nif (i > j)\n\tk = i;");
  printf("\nelse\n\tk = j;\n");

  i = 3; j = 4;
  if (i > j)
    k = i;
  else
    k = j;
  printf("i = %d, j = %d, k = %d\n", i, j, k);

  // here is another way to write this using the ? operator
  printf("\n\nA more cryptic way to write ifs:\nk = (i > j) ? i : j;\n");
  k = (i > j) ? i : j;
  printf("i = %d, j = %d, k = %d\n", i, j, k);

  return (0);

} // end of main


// end of program_control.c
