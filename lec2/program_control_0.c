// program_control_0.c
// Author: Dongwook Shin & Dan Mechanic
// This program illustrates the syntax of assignments
// Simple mathematical operators


#include <stdio.h>

int main()
{ 
  int  i, j, k, l;                   // integer looping variables
  int  iThree = 3;                // integer variable declaration and assignment
  double dZero, dY;               // double precision variables 
  const double PI = 3.141592654;  // double precision constant
                                  // the program will generate an error if an
                                  // attempt is made to change the value of PI



  dZero = dY = 0.0;               // both variables are assigned the value 0
                                  // assignments are executed from right to left
                                  /* comments can also be done this way */

  printf("\nPI is %f\n", PI);

  printf("\ndZero = dY = 0.0\n");
  printf("\ndZero = %f\ndY = %f\n", dZero, dY);
  //
  // arithmetic statements
  //
  printf("\niThree = %d\n", iThree);

  iThree = iThree + 2;
  printf("after iThree + 2; is %d\n", iThree);

  iThree += 2;    //  shorthand for iThree = iThree + 2
  printf("after iThree += 2; is %d\n", iThree);

  iThree *= 3;    //  shorthand for iThree = iThree * 3
  printf("after iThree *= 3; is  %d\n", iThree);

  iThree++;       //  shorthand for iThree = iThree + 1
  printf("after iThree++; iThree is %d\n", iThree);

  iThree--;       //  shorthand for iThree = iThree - 1
  printf("after iThree--; iThree is %d\n", iThree);

  // more on ++
  // hint: don't write code where the order of ++ matters!
  printf("\nMore on ++\n");
  printf("iThree = %d\n", iThree);
  printf("i = ++iThree;");    // increments iThree first then assigns to i
  i = ++iThree;    // increments iThree first then assigns to i
  printf("i = %d, iThree = %d\n", i, iThree);
 
  printf("iThree = %d\n", iThree);
    
  printf("i = iThree++;");    // assigns iThree to i and then increments iThree
  i = iThree++;    // assigns iThree to i and then increments iThree

  printf("i = %d, iThree = %d\n", i, iThree);

  // the modulus (%) operator
  printf("\nThe modulus (%%) operator\n");

  printf("i = 4;j = 11;k = j %% i;l = j / i\n");
  i = 4;
  j = 11;
  k = j % i;   // k is the remainder when j is divided by i
  l = j / i;   // k is the remainder when j is divided by i
  printf("i = %d, j = %d, k = %d, l = %d \n", i, j, k, l);

  return (0);

} // end of main


// end of program_control.c
