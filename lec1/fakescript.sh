#!/bin/bash

# A fake script to illustrate an anecdote
DISEASE=$1;

for i in `seq 1990 2010`;
do
    echo "${DISEASE}      ${i}          ${RANDOM}";
done
